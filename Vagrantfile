# -*- mode: ruby -*-
# vim: set ft=ruby :
# -*- mode: ruby -*-
# vim: set ft=ruby :

MACHINES = {
  :inetRouter => {
    :box_name => "centos/6",
    #:public => {:ip => '10.10.10.1', :adapter => 1},
    :net => [
      { ip: "192.168.255.1", adapter: 2, netmask: "255.255.255.252", virtualbox__intnet: "router-net" },
    ],
  },
  :centralRouter => {
    :box_name => "centos/7",
    :net => [
      { ip: "192.168.255.2", adapter: 2, netmask: "255.255.255.252", virtualbox__intnet: "router-net" },
      { ip: "192.168.0.1", adapter: 3, netmask: "255.255.255.240", virtualbox__intnet: "dir-net" },
      { ip: "192.168.0.33", adapter: 4, netmask: "255.255.255.240", virtualbox__intnet: "hw-net" },
      { ip: "192.168.0.65", adapter: 5, netmask: "255.255.255.192", virtualbox__intnet: "mgt-net" },
      { ip: "192.168.253.1", adapter: 6, netmask: "255.255.255.252", virtualbox__intnet: "router1-net" },
      { ip: "192.168.254.1", adapter: 7, netmask: "255.255.255.252", virtualbox__intnet: "router2-net" },
      { ip: "192.168.252.1", adapter: 8, netmask: "255.255.255.252", virtualbox__intnet: "router3-net" },

    ],
  },

  :centralServer => {
    :box_name => "centos/7",
    :net => [
      { ip: "192.168.0.2", adapter: 2, netmask: "255.255.255.240", virtualbox__intnet: "dir-net" },
    ],
  },

  # :officeRouter1 => {
  #   :box_name => "centos/7",
  #   :net => [
  #     { ip: "192.168.253.2", adapter: 2, netmask: "255.255.255.252", virtualbox__intnet: "router1-net" },
  #     { ip: "192.168.2.1", adapter: 3, netmask: "255.255.255.192", virtualbox__intnet: "dev-net" },
  #   ],
  # },

  # :officeServer1 => {
  #   :box_name => "centos/7",
  #   :net => [
  #     { ip: "192.168.2.2", adapter: 2, netmask: "255.255.255.192", virtualbox__intnet: "dev-net" },
  #   ],
  # },

  # :officeRouter2 => {
  #   :box_name => "centos/7",
  #   :net => [
  #     { ip: "192.168.254.2", adapter: 2, netmask: "255.255.255.252", virtualbox__intnet: "router2-net" },
  #     { ip: "192.168.1.1", adapter: 3, netmask: "255.255.255.192", virtualbox__intnet: "dev-net" },
  #   ],
  # },

  # :officeServer2 => {
  #   :box_name => "centos/7",
  #   :net => [
  #     { ip: "192.168.1.2", adapter: 2, netmask: "255.255.255.192", virtualbox__intnet: "dev-net" },
  #   ],
  # },

  :inetRouter2 => {
    :box_name => "centos/7",
    :net => [
      { ip: "192.168.252.2", adapter: 2, netmask: "255.255.255.252", virtualbox__intnet: "router3-net" },
    ],
  },
}

Vagrant.configure("2") do |config|
  MACHINES.each do |boxname, boxconfig|
    config.vm.define boxname do |box|
      box.vm.box = boxconfig[:box_name]
      box.vm.host_name = boxname.to_s

      boxconfig[:net].each do |ipconf|
        box.vm.network "private_network", **ipconf
      end

      if boxconfig.key?(:public)
        box.vm.network "public_network", boxconfig[:public]
      end

      box.vm.provision "shell", inline: <<-SHELL
          mkdir -p ~root/.ssh
                cp ~vagrant/.ssh/auth* ~root/.ssh
        SHELL

      case boxname.to_s
      when "inetRouter"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
            sysctl net.ipv4.conf.all.forwarding=1
            echo "192.168.0.0/16 via 192.168.255.2 dev eth1" >> /etc/sysconfig/network-scripts/route-eth1
            ifdown eth1 && ifup eth1
            sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
            service sshd restart
            iptables-restore < /vagrant/inetrouter.rules
            SHELL
      when "centralRouter"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo net.ipv4.conf.all.forwarding=1 >> /etc/sysctl.conf
            echo net.ipv4.ip_forward = 1 >> /etc/sysctl.conf
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0 
            echo "GATEWAY=192.168.255.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
            echo "192.168.2.0/24 via 192.168.253.2 dev eth5" >> /etc/sysconfig/network-scripts/route-eth5
            echo "192.168.1.0/24 via 192.168.254.2 dev eth6" >> /etc/sysconfig/network-scripts/route-eth6
            ifdown eth0 && ifup eth0
            ifdown eth1 && ifup eth1
            ifdown eth5 && ifup eth5
            ifdown eth6 && ifup eth6
            ifdown eth7 && ifup eth7
            systemctl restart network
            yum install nmap nc -y
            chmod +x /vagrant/knock.sh
            SHELL
      when "centralServer"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
            # install nginx
            yum install epel-release -y
            yum install nginx -y
            # customize networking
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0 
            echo "GATEWAY=192.168.0.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
            ifdown eth0 && ifup eth0
            ifdown eth1 && ifup eth1
            systemctl restart network
            systemctl enable nginx
            systemctl start nginx
            SHELL
      when "officeRouter1"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0 
            sysctl net.ipv4.conf.all.forwarding=1
            echo "GATEWAY=192.168.253.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
            ifdown eth0 && ifup eth0
            ifdown eth1 && ifup eth1
            systemctl restart network
            SHELL
      when "officeServer1"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0 
            echo "GATEWAY=192.168.2.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
            ifdown eth0 && ifup eth0
            ifdown eth1 && ifup eth1
            systemctl restart network
            SHELL
      when "officeRouter2"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
            sysctl net.ipv4.conf.all.forwarding=1
            echo "GATEWAY=192.168.254.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
            ifdown eth0 && ifup eth0
            ifdown eth1 && ifup eth1
            systemctl restart network
            SHELL
      when "officeServer2"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
            echo "GATEWAY=192.168.1.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
            ifdown eth0 && ifup eth0
            ifdown eth1 && ifup eth1
            systemctl restart network
            SHELL
      when "inetRouter2"
        box.vm.provision "shell", run: "always", inline: <<-SHELL
            sysctl net.ipv4.conf.all.forwarding=1
            echo "GATEWAY=192.168.252.1" >> /etc/sysconfig/network-scripts/ifcfg-eth1
            echo "DEFROUTE=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
            ip route add 192.168.0.0/16 via 192.168.252.1
            ifdown eth0 && ifup eth0
            ifdown eth1 && ifup eth1
            systemctl restart network
            iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 8080 -j DNAT --to-destination 192.168.0.2:80
            iptables -t nat -A POSTROUTING -d 192.168.0.2 -p tcp -m tcp --dport 80 -j SNAT --to-source 192.168.252.2
        SHELL
        box.vm.network "forwarded_port", guest: 8080, host: 28080,
                                         protocol: "tcp", auto_correct: true
      end
    end
  end
end
