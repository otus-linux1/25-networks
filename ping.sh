#! /usr/bin/env bash

hosts=(
  192.168.0.2  # central server
  192.168.0.1  # central router
  192.168.1.2  # office2 server
  192.168.1.1  # office2 router
  192.168.2.2  # office1 server
  192.168.2.1  # office 1 router
  192.168.255.1  # inet router
  8.8.8.8        # google
  ya.ru          # be a patriot! 
)


for line in ${hosts[@]}; do
  ping -w 5 -c1 ${line} &>/dev/null
  ret=$?
  if [ $ret -ne 0 ]; then
    echo "ERROR: host ${line} unreachable"
  else
    echo "OK: ${line}"
  fi
done
