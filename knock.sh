#!/bin/bash
HOST=$1
shift
for PORT in "$@"
do
    nmap -Pn --host-timeout 100 --max-retries 0 -p $PORT $HOST
done
