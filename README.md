# otus-linux
Vagrantfile - для стенда урока 9 - Network

## Дано
Vagrantfile с начальным  построением сети
inetRouter
centralRouter
centralServer

тестировалось на virtualbox

## Планируемая архитектура
построить следующую архитектуру

Сеть office1
- 192.168.2.0/26    - dev
- 192.168.2.64/26   - test servers
- 192.168.2.128/26  - managers
- 192.168.2.192/26  - office hardware

Сеть office2
- 192.168.1.0/25    - dev
- 192.168.1.128/26  - test servers
- 192.168.1.192/26  - office hardware


Сеть central
- 192.168.0.0/28   - directors
- 192.168.0.32/28  - office hardware
- 192.168.0.64/26  - wifi

```
Office1 ---\
-----> Central --IRouter --> internet
Office2----/
```
Итого должны получится следующие сервера
- inetRouter
- centralRouter
- office1Router
- office2Router
- centralServer
- office1Serverfor host in centralRouter centralServer inetRouter officeRouter1 officeRouter2 officeServer1 officeServer2; do
  echo "check ${host}"
  vagrant ssh $host -c 'bash /vagrant/ping.sh'
done
- office2Server

## Теоретическая часть
- Найти свободные подсети
- Посчитать сколько узлов в каждой подсети, включая свободные
- Указать broadcast адрес для каждой подсети
- проверить нет ли ошибок при разбиении

## Практическая часть
- Соединить офисы в сеть согласно схеме и настроить роутинг
- Все сервера и роутеры должны ходить в инет черз inetRouter
- Все сервера должны видеть друг друга
- у всех новых серверов отключить дефолт на нат (eth0), который вагрант поднимает для связи
- при нехватке сетевых интервейсов добавить по несколько адресов на интерфейс


## Решение

### Теория

| ## | Сеть             | Бродкаст      | Кол-во узлов* | Сегмент           |
|----|------------------|---------------|---------------|-------------------|
|  1 | 192.168.2.0/26   |192.168.2.63   | 62            | office1/dev       |
|  2 | 192.168.2.64/26  |192.168.2.127  | 62            | office1/test      |
|  3 | 192.168.2.128/26 |192.168.2.191  | 62            | office1/managers  |
|  4 | 192.168.2.192/26 |192.168.2.255  | 62            | office1/hardware  |
|    |                  |               |               |                   |
|  5 | 192.168.1.0/25   |192.168.1.127  | 126           | office2/dev       |
|  6 | 192.168.1.128/26 |192.168.1.191  | 62            | office2/test      |
|  7 | 192.168.1.192/26 |192.168.1.255  | 62            | office2/hardware  |
|    |                  |               |               |                   |
|  8 | 192.168.0.0/28   |192.168.0.15   | 14            | central/directors |
|  9 | 192.168.0.32/28  |192.168.0.47   | 14            | central/hardware  |
| 10 | 192.168.0.64/26  |192.168.0.127  | 62            | central/wifi      |
|    |                  |               |               |                   |
| 11 | 192.168.0.16/28  |192.168.0.31   | 14            | central/reserved  |
| 12 | 192.168.0.48/28  |192.168.0.63   | 14            | central/reserved  |
| 13 | 192.168.0.128/25 |192.168.0.255  | 126           | central/reserved  |

`*` - исключены адрес сети и бродкаст-адрес

Ошибок не увидел, вроде ничего не перекрывается.

### Практика

Чтобы после развертывания виртуалок настройки таки применились
добавил в вагрантфайле вот такое `ifdown eth0 && ifup eth0`

### Что сделано

Если честно, то я толком не распарсил вот эту схему:

```
Office1 ---\
-----> Central --IRouter --> internet
Office2----/
```

и сделал так, как её понял, а именно вот так:

![network.png](images/network.png)

Приняты следующие решения, допущения и упрощения:

Я решил, что так как на `officeRouter1` и `officeRouter2`
есть только известные сети, будет уместно просто включить там форвардинг
и прописать gateway на `centralRouter`, который в итоге и будет
разбираться что куда.

Подсети в маршрутах на `centralRouter` и `inetRouter` укрупнены,
чтобы писать меньше правил.

<details>
<summary>Шпаргалки для себя</summary>

```
sudo tcpdump -i eth1 icmp

# на inetRouter
ip route add 192.168.0.0/22 via 192.168.255.2 dev eth1

# на centralRouter:
ip route add 192.168.2.0/24 via 192.168.254.2 dev eth5  # office 1
ip route add 192.168.1.0/24 via 192.168.253.2 dev eth6  # office 2
```
</details>

## Как проверять

```
vagrant up
./run_ping.sh
```

`run_ping.sh` проходится по виртуалкам и запускает внутри
скрипт с пингом

зы. В задании предлагалось завести несколько ip на один интерфейс,
но я не понял где :)

## iptables

Схема изменилась и приобрела вот такой вид (но офисы я отключил в вагрантфайле,
а то долго поднимается стенд).

![network.png](images/network-iptables.png)

### knocking port

Скрипт для клиента и правила iptables для сервера
взяты из статьи, приведенной в материалах к лекции, единственное что там
изменилось - `FORWARD DROP` поменял на на `FORWARD ACCEPT`.

#### проверка

```
$ vagrant ssh centralRouter

$ # не понял почему, но от обычного пользователя не хочет открываться
$ # и с sudo тоже иногда открывается только со второго "стука" :shrug:
$ sudo /vagrant/knock.sh 192.168.255.1 8881 7777 9991
$ ssh vagrant@192.168.255.1  # пароль vagrant
```

### nginx

На `centralServer` устновлен `nginx`, на `inetRouter2` настроены следующие правила
для форвардинга запросов

```
iptables -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 8080 -j DNAT --to-destination 192.168.0.2:80
iptables -t nat -A POSTROUTING -d 192.168.0.2 -p tcp -m tcp --dport 80 -j SNAT --to-source 192.168.252.2
```

Сам `inetRouter2` хотит в интернет через `inetRouter1`
```
[vagrant@inetRouter2 ~]$ tracepath -n ya.ru
 1?: [LOCALHOST]                                         pmtu 1500
 1:  192.168.252.1                                         1.014ms 
 1:  192.168.252.1                                         0.481ms 
 2:  192.168.255.1                                         0.536ms 
 ...
 ```

#### Проверка

c хоста выполнить 
```
curl localhost:28080
```
