#! /usr/bin/env bash

for host in centralRouter centralServer inetRouter officeRouter1 officeRouter2 officeServer1 officeServer2; do
  echo "check ${host}"
  vagrant ssh $host -c 'bash /vagrant/ping.sh'
done
